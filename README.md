This is an easy Test created by Ishpreet Singh.

Hope you will like it.

------------------------
	GIT CHALLENGE
------------------------

We will start with the git task first.

Q. What's your Name?

A. My Name is _____________.



1. Clone the repository
2. Create a branch with your name.
3. Fill the above blank with your Name.
4. Push your branch.
5. Make sure you will have only one commit (Hint: git log to see all commits, Make sure it contains only your commit, by default it will have a initial commit with message **INITIAL COMMIT**. So after adding your name it should have total of one commit).


-----------------------
	MYSQL CHALLENGE
-----------------------

This is a challenge on hackerrank.
https://www.hackerrank.com/challenges/the-report/problem


---------------------------
	Javascript Challenge
---------------------------

This is a challenge on hackerrank.
https://www.hackerrank.com/challenges/pangrams/problem

-------------------------
	Node JS Challenge
-------------------------

Do the challenges Time Server and Juggling Async from learnyounode.
Note: To install learnyounode Make sure you have npm installed on your local machine and use this command:	npm install -g learnyounode.


**NOTE** for all challenges create a new file with challenge name and add your solution. For hackerrank question share your hackerrank username as well as along with your solution. All the challenge is to be done in your repo with only one commit.


------------------------
	Marking Scheme
------------------------

Git Challenge 	- 15
Mysql Challenge - 15
Javascript 		- 10
NodeJs 			- 10

------------------------
Total 			- 50

All the Best:)